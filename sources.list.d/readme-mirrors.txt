
mx.list

# MX Community Main and Test Repos
deb http://mxrepo.com/mx/repo/ bookworm main non-free
#deb http://mxrepo.com/mx/testrepo/ bookworm test
#deb http://mxrepo.com/mx/repo/ bookworm ahs

debian-stable-updates.list

# Debian Stable Updates
deb http://deb.debian.org/debian/ bookworm-updates main contrib non-free non-free-firmware

debian.list

# Debian Stable
deb http://deb.debian.org/debian bookworm main contrib non-free
deb http://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware
#deb-src http://deb.debian.org/debian bookworm main contrib non-free


Source:
https://mxlinux.org/uncategorized/repos-mx-23/

Related links:

the status of MX Linux mirrors
http://rsync-mxlinux.org/mirmon/

Package download mirrors
https://mxlinux.org/wiki/system/package-download-mirrors/
