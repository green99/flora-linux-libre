
How to change apps locale temporarily during login-session

Run the following command from MATE Terminal.
The example below set the present language for Pluma editor into Japanese.

demo@mx1:~
$ LANG=ja_JP.UTF-8 pluma &
