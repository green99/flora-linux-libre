
root@mx1:/home/demo# dpkg-reconfigure tzdata
debconf: unable to initialize frontend: Gnome
debconf: (Can't locate Gtk3.pm in @INC (you may need to install the Gtk3 module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.36.0 /usr/local/share/perl/5.36.0 /usr/lib/x86_64-linux-gnu/perl5/5.36 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl-base /usr/lib/x86_64-linux-gnu/perl/5.36 /usr/share/perl/5.36 /usr/local/lib/site_perl) at /usr/share/perl5/Debconf/FrontEnd/Gnome.pm line 151.)
debconf: falling back to frontend: Dialog
debconf: unable to initialize frontend: Dialog
debconf: (No usable dialog-like program is installed, so the dialog based frontend cannot be used. at /usr/share/perl5/Debconf/FrontEnd/Dialog.pm line 78.)
debconf: falling back to frontend: Readline
Configuring tzdata
------------------

あなたの居住する地理的領域を選んでください。続く設定質問で、位置する時間帯を表現
する都市名のリストが表示されるので、これをより狭めていくことができます。

  1. アフリカ  5. アジア              9. インド地方
  2. アメリカ  6. 大西洋側           10. 太平洋側
  3. 南極大陸  7. オーストラリア  11. US
  4. 北極        8. ヨーロッパ        12. その他

地理的領域: 5


あなたの時間帯に一致する都市または地域を選択してください。

  1. アデン               45. クラスノヤルスク
  2. アルマトゥイ      46. クアラルンプール
  3. アンマン            47. クチン
  4. アナディル         48. クウェート
  5. アクタウ            49. マカオ
  6. アクトベ            50. マガダン
  7. アシガバード      51. マカッサル
  8. アティラウ         52. マニラ
  9. バグダード         53. マスカット
  10. バーレーン        54. ニコシア
  11. バクー              55. ノヴォクズネツク
  12. バンコク           56. ノヴォシビルスク
  13. バルナウル        57. オムスク
  14. ベイルート        58. ウラル
  15. ビシュケク        59. ブノンペン
  16. ブルネイ           60. ポンティアナク
  17. チタ                 61. 平壌
  18. チョイバルサン  62. カタール
[More] 

  19. 重慶                 63. コスタナイ
  20. コロンボ           64. キジルオルダ
  21. ダマスカス        65. リヤド
  22. ダッカ              66. サハリン
  23. ディリ              67. サマルカンド
  24. ドバイ              68. ソウル
  25. ドゥシャンベ     69. 上海
  26. ファマグスタ     70. シンガポール
  27. ガザ                 71. スレドネコリムスク
  28. ハルビン           72. 台北
  29. ヘブロン           73. タシケント
  30. ホーチミン        74. トビリシ
  31. 香港                 75. テヘラン
  32. ホブド              76. テルアビブ
  33. イルクーツク     77. ティンプー
  34. イスタンブール  78. 東京
  35. ジャカルタ        79. トムスク
  36. ジャヤプラ        80. ウランバートル
  37. エルサレム        81. ウルムチ
  38. カブール           82. ウスチ=ネラ
  39. カムチャッカ     83. ヴィエンチャン
  40. カラチ              84. ウラジオストック
[More] 

  41. カシュガル        85. ヤクーツク
  42. カトマンズ        86. ヤンゴン
  43. ハンドゥイガ     87. エカテリンブルグ
  44. コルコタ           88. エレヴァン

時間帯: 78



Current default time zone: 'Asia/Tokyo'
Local time is now:      Mon Feb 12 20:01:47 JST 2024.
Universal Time is now:  Mon Feb 12 11:01:47 UTC 2024.

root@mx1:/home/demo# exit
demo@mx1:~
$ date
2024年  2月 12日 月曜日 20:20:09 JST
demo@mx1:~
$ 
