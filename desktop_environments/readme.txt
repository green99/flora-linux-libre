MX Package Installer is the most convenient tool to add extra Desktop Environment.
But you should be careful for its default behavior of package selections.
Sometimes, a choice of item is missing some packages or install improper packages for its dependency.

To avoid this issue, you can install manually by APT command instead.

Open /usr/share/mx-packageinstaller-pkglist which includes respective package lists. Then you can know
specific commands. For example, budgie-desktop.pm shows as below:


<preinstall>
</preinstall>

<install_package_names>
budgie-desktop
gnome-tweaks
budgie-desktop-doc
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
budgie-desktop
gnome-tweaks
budgie-desktop-doc
</uninstall_package_names>

In this case, MX Package Installer runs internally the following APT command:

apt install budgie-desktop gnome-tweaks budgie-desktop-doc

If you want to uninstall afterwards, run as below:

apt remove budgie-desktop gnome-tweaks budgie-desktop-doc

END.
