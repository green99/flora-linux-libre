
FSFLA/ selibre/ linux-libre/ GNU Linux-libre's Freesh project

    Edit
    RecentChanges
    Preferences
    ?Discussion

    en

Home Announcements GNU Linux-libre Events Legislation

FSFLA

    About FSFLA
    Constitution
    Participate
    Mailing lists
    History

Sister FSFs

    FSF
    FSF Europe
    FSF France
    FSF India

Links

    Members' blogs
    Ikiwiki documentation

Linux, as distributed by Linus Torvalds et al, contains non-Free Software, i.e., software that does not respect your essential freedoms, and it induces you to install additional non-Free Software that it doesn't contain.

GNU Linux-libre is a project to maintain and publish 100% Free versions of Linux, removing the offending portions.
Freesh

Freesh is a sub-project that contains .debs of Linux-libre compiled for general purpose use on 64-bit ARM, 32-bit ARMv7 with hardware FPU, 32- and 64-bit x86, 64-bit RISC-V, the Motorola 68000 series (68020 or later; an MMU is required), Loongson 2F MIPS-compatible processors (such as the Fuloong Mini-PC and the Lemote Yeeloong laptop), OpenRISC 1000, OpenSPARC, IBM Z and LinuxONE mainframes, as well as 32- and 64-bit Power Architecture processors.

Please write to the mailing list or visit #gnu-linux-libre on irc.libera.​chat if you need support for additional CPU architectures.

If you need a realtime kernel see the libeRTy APT repository.

If you need kernels packaged for RPM please see the RPM Freedom page.
How To Use

To use Freesh first check for a compatible architecture:

dpkg --print-architecture 

You should see one of the following:

amd64
arm64
armhf
i386
m68k
mipsel
or1k
powerpc
ppc64
ppc64el
riscv64
s390x
sparc64

Only if you see i386, determine if your i386 CPU supports PAE (which is good to know for later) run this command. If your architecture is not listed as i386 then skip this step:

grep --color=always -i PAE /proc/cpuinfo 

If pae is highlighted in the output then your system supports PAE. Otherwise it does not.

If you have a compatible architecture first download the archive keyring package:

wget https://linux-libre.fsfla.org/pub/linux-libre/freesh/pool/main/f/freesh-archive-keyring/freesh-archive-keyring_1.1_all.deb

This archive keyring package installs the GPG public keys for Jason Self and Alexandre Oliva in /usr/share/keyrings/freesh-archive-keyring, the repository configuration in /etc/apt/sources.list.d/freesh.sources, and the apt preferences files in /etc/apt/preferences.d/freesh.pref.

At this point you may, at your option, inspect the archive keyring package to your satisfaction with commands such as:

dpkg -c freesh-archive-keyring_1.1_all.deb

Install it:

sudo dpkg -i freesh-archive-keyring_1.1_all.deb

You can also check that the right keys are installed:

gpg --no-default-keyring --keyring /usr/share/keyrings/freesh-archive-keyring.gpg --list-keys

Make sure that you see the fingerprints:

F611 A908 FFA1 65C6 9958 4ED4 9D0D B31B 545A 3198

A8CA A4A2 EB65 5D07 BA1F 367B C338 CAA4 FA70 0A3A

Remove the temporary copy:

rm freesh-archive-keyring_1.1_all.deb

Now you will now be able to update your package manager and install Linux-libre:

sudo apt update

Next, decide what you want to do:

Short-term or long-term support?

    Short-term support (STS) versions provides all of the latest changes and features but are only supported for about 2-3 months so you're upgrading to new a new stable version more often.
    Long-term support (LTS) versions are suported for at least 2 years but won't necessarily have the latest stuff. If you want to use Linux-libre and prefer a kernel that isn't changing as much, the long-term versions are probably what you want.

If you want the latest kernel version:

sudo apt install linux-libre

If you want to use a long-term support (LTS) kernel version:

sudo apt install linux-libre-lts

If you have an older computer that doesn't support PAE (see the earlier note about PAE) you can install the nonpae versions of these instead by adding -nonpae to the end of the package name (i.e. linux-libre-nonpae or linux-libre-lts-nonpae.)

In some very specific cases you may also want to install the kernel headers, like if you're building out of tree kernel modules. In such cases, for whatever package you installed above, also install the same one with -headers.

In some other cases you may want to follow a specific kernel version. To see a list of currently supported kernel packages:

apt list | grep -i linux-libre-

You may then install those instead.

Have questions? Need help? Please write to the mailing list or visit #gnu-linux-libre on irc.libera.​chat.
Sources

Linux-libre is free software, licensed under the terms of the GNU General Public License version 2, as published by the Free Software Foundation. You should read the license so that you know your rights to run, study, and modify the software, as well as your obligations should you redistribute the software to others.

You may download the tarball containing the complete and corresponding source code, kernel configuration files, and the instructions to compile and install the kernel from https://linux-libre.fsfla.org/pub/linux-libre/freesh/
Be Free!

Page text is copyright © Jason Self. You can redistribute and/or modify this text under terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. Please copy and share.

As a special exception, you may distribute this work without including the copy of the GPL that would normally be required as long as you keep intact all notices stating that this license and special exception apply, and include a URI or hyperlink to a public copy of the GPL. If you modify this work, you may extend this exception to your version of the work, but you are not obligated to do so. If you do not wish to do so, delete this exception statement from your version.
Links: index.en liberty.en rpmfreedom.en
Last edited Fri Feb 3 04:50:57 2023

